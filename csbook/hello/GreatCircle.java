/* *****************************************************************************
 *  Name:              Ada Lovelace
 *  Coursera User ID:  123456
 *  Last modified:     October 16, 1842
 **************************************************************************** */

public class GreatCircle {
    public static void main(String[] args) {
        double x1 = Math.toRadians(Double.parseDouble(args[0]));
        double y1 = Math.toRadians(Double.parseDouble(args[1]));
        double x2 = Math.toRadians(Double.parseDouble(args[2]));
        double y2 = Math.toRadians(Double.parseDouble(args[3]));

        double left = Math.pow(Math.sin(((x2 - x1) / 2)), 2);
        double right = Math.pow(Math.sin(((y2 - y1) / 2)), 2);

        double mid = (Math.cos(x1) * Math.cos(x2));
        double sq = Math.sqrt((left + (mid * right)));

        double r = 6371.0;
        double distance = ((2 * r) * Math.asin(sq));

        System.out.println(distance);

    }
}
