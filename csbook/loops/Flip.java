public class Flip {
    public static void main(String[] args) { //simulate coin flip
        if (Math.random() < 0.5) System.out.println("Heads");
        else System.out.println("Tails");
    }
}
